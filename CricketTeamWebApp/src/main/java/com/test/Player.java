package com.test;

public class Player {
	
	 int id;
	 String name;
     int matches;
     int runs;
     int wickets;
     int ducks;
     String position;
     double avgScore;
     int highScore;
 

     public Player()
     {
    	 super();
     }
     
     public Player(int id, String name, int matches, int runs, int wickets, int ducks, String position, double avgScore, int highScore)
     {
         this.id = id;
         this.name = name;
         this.matches = matches;
         this.runs = runs;
         this.wickets = wickets;
         this.ducks = ducks;
         this.position = position;
         this.avgScore = avgScore;
         this.highScore = highScore;
     }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMatches() {
		return matches;
	}

	public void setMatches(int matches) {
		this.matches = matches;
	}

	public int getRuns() {
		return runs;
	}

	public void setRuns(int runs) {
		this.runs = runs;
	}

	public int getWickets() {
		return wickets;
	}

	public void setWickets(int wickets) {
		this.wickets = wickets;
	}

	public int getDucks() {
		return ducks;
	}

	public void setDucks(int ducks) {
		this.ducks = ducks;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public double getAvgScore() {
		return avgScore;
	}

	public void setAvgScore(double avgScore) {
		this.avgScore = avgScore;
	}

	public int getHighScore() {
		return highScore;
	}

	public void setHighScore(int highScore) {
		this.highScore = highScore;
	}
	
	
     
}
