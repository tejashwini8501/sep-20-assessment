package com.test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class PlayerDao {
	
	public static int savePlayer(Player player) {
		Statement stmt = null;
		String sql = null;
		try {
			sql = "insert into player values("+player.getId()+", '"+player.getName()+"', "+player.getMatches()+", "+player.getRuns()+", "+player.getWickets()+", "+player.getDucks()+", '"+player.getPosition()+"', "+player.getAvgScore()+", "+player.getHighScore()+")";
			stmt = Util.getConnection().createStatement();
			return stmt.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	public static int updatePlayer(Player player)
	{
		Statement stmt = null;
		String sql = null;
		try {
			sql = "update player set Name = "+player.getName()+", Matches =  "+player.getMatches()+", Runs = "+player.getRuns()+", Wickets = "+player.getWickets()+", Position = '"+player.getPosition()+"', Avg_Score = "+player.getAvgScore()+",High_Score = "+player.getHighScore()+" where ID = "+player.getId()+"";
			stmt = Util.getConnection().createStatement();
			return stmt.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	public static int deletePlayer(Player player)
	{
		Statement stmt = null;
		String sql = null;
		try {
			sql = "delete from player where ID = "+player.getId()+"";
			stmt = Util.getConnection().createStatement();
			return stmt.executeUpdate(sql);
		}
		catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	public static Player getPlayerById(int id) {
		Player player = new Player();
		Statement st = null;
		try {
			String sql = "select * from player where id = "+player.getId()+"";
			st = Util.getConnection().createStatement();
			ResultSet rs = st.executeQuery(sql);
			if(rs.next()) {
				player.setId(rs.getInt(1));
				player.setName(rs.getString(2));
				player.setMatches(rs.getInt(3));
				player.setRuns(rs.getInt(4));
				player.setWickets(rs.getInt(5));
				player.setDucks(rs.getInt(6));
				player.setPosition(rs.getString(7));
				player.setHighScore(rs.getInt(8));
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return player;
		
	}
	
	public List<Player> getAllPlayers(){
		List<Player> lst = new ArrayList<Player>();
		Statement st = null;
		try {
			String sql = "select * from player";
			st = Util.getConnection().createStatement();
			ResultSet rs = st.executeQuery(sql);
			while(rs.next()){
				Player player = new Player();
				player.setId(rs.getInt(1));
				player.setName(rs.getString(2));
				player.setMatches(rs.getInt(3));
				player.setRuns(rs.getInt(4));
				player.setWickets(rs.getInt(5));
				player.setDucks(rs.getInt(6));
				player.setPosition(rs.getString(7));
				player.setHighScore(rs.getInt(8));
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return lst;
	}
	
}
