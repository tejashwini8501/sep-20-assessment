package com.test;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/UpdatePlayer")
public class UpdatePlayer extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");	
		PrintWriter out = response.getWriter();
		String id = (String)request.getParameter("id");
		String name = (String)request.getParameter("name");
		String matches = (String)request.getParameter("matches");
		String runs = (String)request.getParameter("runs");
		String wickets =(String)request.getParameter("wickets");
		String ducks = (String)request.getParameter("ducks");
		String position =(String)request.getParameter("position");
		String highscore = (String)request.getParameter("highscore");
		
		double avgScore = (double) Integer.parseInt(runs) / Integer.parseInt(matches);
		Player player = new Player();
		player.setId(Integer.parseInt(id));
		player.setName(name);
		player.setMatches(Integer.parseInt(matches));
		player.setRuns(Integer.parseInt(runs));
		player.setWickets(Integer.parseInt(wickets));
		player.setDucks(Integer.parseInt(ducks));
		player.setPosition(position);
		player.setHighScore(Integer.parseInt(highscore));
		player.setAvgScore(avgScore);
		
		int x = PlayerDao.updatePlayer(player);
		 if(x>0)
		 {
			 RequestDispatcher rd = request.getRequestDispatcher("updatePlayer.html");
			 rd.include(request, response);
			 out.println("<center><font color = 'green'; weight = bolder;>Successfully updated a player.</font></center>");
		 }
		 else
		 {
			 out.println("Invalid details");
		 }
		 
	}


}
